#pragma once

#include "vector.hh"
#include "../api/OpenGL_API.hh"

using drawNS::Point3D;

class Vector3D : public Vector<double,3>{
private:
    
public:
    static int akt;
    static int ogolne;
    
    Vector3D();
    Vector3D(double a, double b, double c);
    // Vector3D(const Vector3D&& vector) = delete;
    // Vector3D& operator = (const Vector3D&& vector) = default;
    Vector3D(const Vector3D& vector); // Not default
    Vector3D& operator = (const Vector3D& vector);
    Vector3D(const Vector<double,3>& vector);
    Vector3D(std::initializer_list<double> in);
    ~Vector3D();
    Point3D toPoint3D() const;
    
};

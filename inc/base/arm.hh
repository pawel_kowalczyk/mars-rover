#pragma once

#include "cuboid.hh"

class Arm : public Cuboid{
private:

public:
    Arm() = default;
    Arm(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, double len);
};
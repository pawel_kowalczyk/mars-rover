#pragma once

#include "rover.hh"
#include "SFJRoverInterface.hh"

class SFJRover : public Rover, public SFJRoverInterface{

private:
    Cuboid head;
    Cuboid arm1, arm2;

public:
    SFJRover() : Rover(){};
    SFJRover(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, string color);


};
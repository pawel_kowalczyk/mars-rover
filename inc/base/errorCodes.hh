#pragma once

#define ERR_DIV_BY_0 -1
#define ERR_IDX_OUT_OF_RANGE -2
#define ERR_READING_SET -3
#define ERR_DET_0 -4
#define ERR_DIAGONAL -5
#define ERR_INVERSE_MATRIX -6
#define ERR_UNKNOWN_DATATYPE -7

#define E_NEGATIVE_SIZE -8
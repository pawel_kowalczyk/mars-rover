#pragma once

#include "../utils/matrixRot.hh"
#include "../utils/vector3D.hh"

class CoordSystem{
private:
    CoordSystem* _parent;
    MatrixRot _rotation;
    Vector3D _translation;
  
public:

    CoordSystem();
    CoordSystem(CoordSystem* parent, MatrixRot rot, Vector3D translation);
    
    void translate(const Vector3D& newTranslation);
    void rotate(const MatrixRot& newRotation);

    void rotateAngleX(double angle);
    void rotateAngleY(double angle);
    void rotateAngleZ(double angle);

    Vector3D calculateToGlobal(const Vector3D& p) const;

    CoordSystem* getParent() const;
    MatrixRot getRotation() const;
    Vector3D getTranslation() const;

};
#pragma once

#include "coordSystem.hh"
#include "../api/OpenGL_API.hh"
#include "renderInterface.hh"

#include <vector>
#include <iostream>

using std::shared_ptr;

class Cuboid : public CoordSystem, public RenderInterface {
private:
    Vector3D _size;
    std::array<Vector3D, 8> _vertexes;

    void calculateVertexes();

public:

    Cuboid() : CoordSystem() {setId(-1);}
    Cuboid(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, double length, double width, double height);    
    
    void render() override;
    
    Vector3D getSize() const { return _size; }
    std::array<Vector3D, 8>& getVertexes() { return _vertexes; }
};
#pragma once

#include "../api/Draw3D_api_interface.hh"
#include "../utils/vector3D.hh"

using std::string;

class RenderInterface{
protected:
    std::shared_ptr<drawNS::Draw3DAPI> api;
    int _id;
    string _color;

public:
    virtual ~RenderInterface() = default;
    virtual void render() = 0;
    virtual int getId() const {return _id;}
    virtual void setId(int newId) {_id = newId;}
    virtual string getColor() const {return _color;}
    virtual void setColor(const string& color) {_color=color;}
};
#pragma once

#include "coordSystem.hh"
#include "../api/OpenGL_API.hh"
#include "renderInterface.hh"

#define WHEEL_THICKNESS 1

using std::shared_ptr;

class Wheel : public CoordSystem, public RenderInterface{
private:
    double _r;
    Vector<Vector3D,20> _vertexes;

    void calculateVertexes();

public:
    Wheel() = default;
    Wheel(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, double r );

    void render() override;
    double getCircumference() const;

};
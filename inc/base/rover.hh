#pragma once

#include <chrono>
#include <thread>

#include "cuboid.hh"
#include "wheel.hh"
#include "regolit.hh"
#include "arm.hh"
#include <algorithm>

#include "roverInterface.hh"
#include "obstacleInterface.hh"
#include "renderInterface.hh"

#define STANDARD_ROVER_SPEED 0.1
#define STANDARD_ROVER_ROTATION 2

#define FPS 50ms

#define ROVER_LENGTH 5
#define ROVER_WIDTH 5
#define ROVER_HEIGHT 1

using namespace std::chrono_literals;
using std::string;
using std::shared_ptr;
using std::cout;

class Rover : public RoverInterface, public RenderInterface, public ObstacleInterface{
private:

protected:
    Cuboid frame;  //pole
    Wheel wheels[4];  // pole
    Arm arm;
    std::vector<RenderInterface*> renderable;

    bool checkCollisions(std::vector<ObstacleInterface*> objects) const override;
    
public:

    Rover();
    Rover(std::shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, string color);
    ~Rover();
    
    void drive(double dist, double velocity, std::vector<ObstacleInterface*> objects) override;
    void rotate(double angle, double speed) override;

    //void update();
    void render() override;

    void updateCollisionRect() override;
    std::array<Vector3D,4> getCorners() const;
};
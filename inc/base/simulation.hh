#pragma once

#include "rover.hh"
#include "SFJrover.hh"
#include "regolit.hh"
#include "../api/OpenGL_API.hh"
#include <iostream>
#include <unistd.h>
#include <malloc.h>


using std::cout;
using std::endl;
using std::cerr;
using std::cin;

#define MAP_SIZE_LENGTH 25
#define MAP_SIZE_WIDTH 25
#define MAP_SIZE_HEIGHT 25

#define REGOLIT_PICK_RANGE 2.8

class Simulation{
private:
    std::shared_ptr<drawNS::Draw3DAPI> api;
    CoordSystem* globalCoords;
    std::vector<Rover*> rovers;
    std::vector<Regolit*> probes;
    Rover* onFocus;

    std::vector<RenderInterface*> renderable;
    std::vector<ObstacleInterface*> obstacles;

    double distance, speed;
    double angle, rotSpeed;

    bool _running;

    void wait4key();
    //void update();
    void render();
    void menu() const;
    void changeRover();
    void highlightRovers() const;    
    void decisionsDrive();
    void decisionsRotate();

    bool canPickRegolit(Regolit* regolit) const;
    void removeRegolit(Regolit* toRemove);
    void tryToPickRegolit();

public:
    Simulation(int* argc, char** argv);

    void loop();

};
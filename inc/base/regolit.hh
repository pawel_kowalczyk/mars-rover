#pragma once

#include "regolitInterface.hh"
#include "renderInterface.hh"
#include "cuboid.hh"
#include "obstacleInterface.hh"


class Regolit : public RegolitInterface, public ObstacleInterface, public Cuboid {
private:

public:
    Regolit();
    Regolit(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, string t_name);

    void updateCollisionRect() override;
    void moveAway();
};
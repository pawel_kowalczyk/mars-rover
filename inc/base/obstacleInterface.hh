#pragma once

#include "../utils/vector3D.hh"

enum CollisionID { C_ROVER=600, C_SFJ_ROVER=601, C_REGOLIT=602 };

struct CollisionParameters{
    Vector3D center;
    double radius;
};

class ObstacleInterface{
protected:
    CollisionID collisionID;
    CollisionParameters collisionParams;
    
    virtual bool checkCollision(const CollisionParameters& obj) const{
        return (collisionParams.center - obj.center).length() < collisionParams.radius + obj.radius;
    }

public:
    virtual ~ObstacleInterface() = default;
    virtual void updateCollisionRect() = 0;
    virtual CollisionParameters getCollisionParameters() const { return collisionParams; }
    virtual Vector3D getCenter() const { return collisionParams.center; }
    virtual double getRadius() const { return collisionParams.radius; }
    virtual CollisionID getCollisionID() const { return collisionID; }
};
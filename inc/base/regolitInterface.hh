#pragma once

#include <string>

using std::string;

class RegolitInterface {
protected:
    string name;

public:
    virtual ~RegolitInterface() = default;
    virtual string getName() const { return name; }

};
#pragma once

#include <array>

#include "obstacleInterface.hh"

class RoverInterface{
protected:

    virtual bool checkCollisions(std::vector<ObstacleInterface*> objects) const = 0; 
    
public:
    virtual ~RoverInterface() = default;
    virtual void drive(double dist, double velocity, std::vector<ObstacleInterface*> objects) = 0;
    virtual void rotate(double angle, double speed) = 0;
};

#include "../../inc/base/coordSystem.hh"

CoordSystem::CoordSystem(){
    _parent = nullptr;
    _rotation = {1,0,0, 0,1,0, 0,0,1};
    _translation = {0,0,0};
}

CoordSystem::CoordSystem(CoordSystem* parent, MatrixRot rot, Vector3D translation) : _parent(parent), _rotation(rot), _translation(translation) {

}

void CoordSystem::translate(const Vector3D& newTranslation){
    _translation += newTranslation;
}

void CoordSystem::rotate(const MatrixRot& newRotation){
    _rotation = newRotation*_rotation;
}

void CoordSystem::rotateAngleX(double angle){
    double radians = angle*M_PI/180;
    MatrixRot rotationMatrix = {1, 0,             0,
                                0, cos(radians), -sin(radians), 
                                0, sin(radians),  cos(radians)};
    rotate(rotationMatrix);
}

void CoordSystem::rotateAngleY(double angle){
    double radians = angle*M_PI/180;
    MatrixRot rotationMatrix = { cos(radians), 0, sin(radians), 
                                 0,            1, 0,
                                -sin(radians), 0, cos(radians),};
    rotate(rotationMatrix);
}

void CoordSystem::rotateAngleZ(double angle){
    double radians = angle*M_PI/180;
    MatrixRot rotationMatrix = {cos(radians), -sin(radians), 0, 
                                sin(radians),  cos(radians), 0,
                                0           ,  0           , 1 };
    rotate(rotationMatrix);
}

Vector3D CoordSystem::calculateToGlobal(const Vector3D& p) const {

    if(_parent != nullptr){
        return _parent->calculateToGlobal(_rotation*p + _translation);
    }
    else
        return _rotation*p + _translation;
}

CoordSystem* CoordSystem::getParent() const{
    return _parent;
}

MatrixRot CoordSystem::getRotation() const{
    return _rotation;
}

Vector3D CoordSystem::getTranslation() const{
    return _translation;
}
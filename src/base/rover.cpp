#include "../../inc/base/rover.hh"

Rover::Rover(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, string color){
    api = t_api;

    collisionID = C_ROVER;

    frame = Cuboid(api, parent, rot, translation, ROVER_LENGTH, ROVER_WIDTH, ROVER_HEIGHT);
    setColor(color);

    // arm = Arm(api, frame.getParent(), MatrixRot('Y', -45), Vector3D(), 6);

    wheels[0] = Wheel(api, &frame, MatrixRot(), Vector3D({ 2, 3.4, -0.5}), 1);
    wheels[1] = Wheel(api, &frame, MatrixRot(), Vector3D({ 2,-2.6, -0.5}), 1);
    wheels[2] = Wheel(api, &frame, MatrixRot(), Vector3D({-2, 3.4, -0.5}), 1);
    wheels[3] = Wheel(api, &frame, MatrixRot(), Vector3D({-2,-2.6, -0.5}), 1);

    for(int i=0; i<4; i++){
        wheels[i].rotateAngleX(90);
        wheels[i].translate({0,0,0.5});
    }

    renderable.push_back(&frame);
    // renderable.push_back(&arm);
    for(int i=0; i<4; i++)
        renderable.push_back(&wheels[i]);

    collisionParams.center = frame.calculateToGlobal({0,0,0});
    collisionParams.radius = Vector3D({6,7,2}).length()/2;
}

Rover::~Rover(){
    renderable.clear();
}

bool Rover::checkCollisions(std::vector<ObstacleInterface*> objects) const{
    for( ObstacleInterface* obj : objects ){
        if ( this->getCenter() != obj->getCenter() ){
            if ( checkCollision( obj->getCollisionParameters() ) ){
                return true;
            }
        }
    }
    return false;
}

void Rover::drive(double dist, double velocity, std::vector<ObstacleInterface*> objects){
    if( velocity < 0 ){
        return;
    }
    else if ( velocity == 0 ){
        velocity = STANDARD_ROVER_SPEED;
    }

    if( dist < 0 )
        velocity *= -1;

    for(double i=0; i<fabs(dist); i+=fabs(velocity)){
        
        frame.translate( frame.getRotation()*Vector3D({velocity,0,0}) );

        for(Wheel& wheel : wheels){
            wheel.rotateAngleY(velocity*180/wheel.getCircumference());
        }

        this->updateCollisionRect();

        if( checkCollisions(objects) ){    
            dist = 0;      
            this->drive( -(dist/fabs(dist))*3, STANDARD_ROVER_SPEED, objects);
        }

        this->render();

        std::this_thread::sleep_for(FPS);
    }
}

void Rover::rotate(double angle, double rotSpeed){

    if( rotSpeed < 0 ){
        return;
    }

    else if ( rotSpeed == 0 ){
        rotSpeed = STANDARD_ROVER_ROTATION;
    }

    if( angle < 0 )
        rotSpeed *= -1;

    for(double i=0; i<fabs(angle); i+=fabs(rotSpeed)){
        frame.rotateAngleZ(rotSpeed);
        wheels[0].rotateAngleY(-rotSpeed);
        wheels[2].rotateAngleY(-rotSpeed);
        wheels[1].rotateAngleY(rotSpeed);
        wheels[3].rotateAngleY(rotSpeed);
    
        this->render();
    
        std::this_thread::sleep_for(FPS);    
    }
}

void Rover::render(){

    for(auto obj : renderable){
        obj->render();
        api->change_shape_color(obj->getId(), _color);
    }    
}

void Rover::updateCollisionRect(){
    collisionParams.center = frame.calculateToGlobal({0,0,0});
}

std::array<Vector3D,4> Rover::getCorners() const{
    return {frame.calculateToGlobal({2.5,2.5,0}),
            frame.calculateToGlobal({-2.5,2.5,0}),
            frame.calculateToGlobal({2.5,-2.5,0}),
            frame.calculateToGlobal({-2.5,-2.5,0})};
}
#include "../../inc/base/regolit.hh"

Regolit::Regolit(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, string t_name) : 
Cuboid(t_api, parent, rot, translation, 2, 0.5, 0.5) {
    
    collisionID = C_REGOLIT;
    
    name=t_name;

    collisionParams.center = calculateToGlobal({0,0,0});
    collisionParams.radius = getSize().length()/2;
}


void Regolit::updateCollisionRect(){
    collisionParams.center = calculateToGlobal({0,0,0});
}

void Regolit::moveAway(){
    translate({100,100,100});
    updateCollisionRect();
}
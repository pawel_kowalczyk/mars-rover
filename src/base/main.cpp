#include "../../inc/base/simulation.hh"

int Vector3D::akt = 0;
int Vector3D::ogolne = 0;

int main(int argc, char** argv){

    Simulation simulation(&argc, argv);
    simulation.loop();

    return 0;
}

/*
    TODO
    !! based on interfaces
    // interfaces //
    GeomFigureInterface
    RenderInterface
    ObstacleInterface
    RoverInterface : RenderInterface, ObstacleInterface 
    SFJRoverInterface

    CoordSystem
    Cuboid   : GeomFigureInterface, RenderInterface, CoordSystem
    Wheel    : GeomFigureInterface, RenderInterface, CoordSystem
    Rover    : RoverInterface
    SFJRover : SFJRoverInterface, Rover
    Regolit  : Cuboid
*/
#include "../../inc/base/SFJrover.hh"

SFJRover::SFJRover(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, string color) : Rover(t_api, parent, rot, translation, color){
    
    collisionID = C_SFJ_ROVER;

    head = Cuboid(t_api, &frame, MatrixRot(), Vector3D({2,0,1}),1,1,1);

    renderable.push_back(&head);
}

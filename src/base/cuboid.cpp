/*
            length
         __________
        |          |\  height
width   |    .(0,0)| |
        |__________| |
         \__________\|
*/
#include "../../inc/base/cuboid.hh"

Cuboid::Cuboid(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation,
               double length, double width, double height) : CoordSystem(parent, rot, translation){
    
    api = t_api;

    if(length<0 || width<0 || height<0){
        std::cerr << " !!! Size can't be a negative number !!! " << std::endl;
        exit(E_NEGATIVE_SIZE);
    }

    _size = Vector3D({length, width, height});
    setId(-1);
    setColor("black");
    
    calculateVertexes();
}

void Cuboid::calculateVertexes() {
    Vector3D hSize = _size/2;

    // Top surface
    _vertexes[0] = {-hSize[0],  hSize[1], hSize[2]};
    _vertexes[1] = { hSize[0],  hSize[1], hSize[2]};
    _vertexes[2] = { hSize[0], -hSize[1], hSize[2]};
    _vertexes[3] = {-hSize[0], -hSize[1], hSize[2]};

    // Bottom surface
    _vertexes[4] = {-hSize[0],  hSize[1], -hSize[2]};
    _vertexes[5] = { hSize[0],  hSize[1], -hSize[2]};
    _vertexes[6] = { hSize[0], -hSize[1], -hSize[2]};
    _vertexes[7] = {-hSize[0], -hSize[1], -hSize[2]};

}

void Cuboid::render(){
    if(getId() != -1){
        api->erase_shape(getId());
    }

    std::vector<drawNS::Point3D> tmp;
    for(int i=0; i<8; i++) 
        tmp.push_back(calculateToGlobal(_vertexes[i]).toPoint3D());
    
    std::vector<drawNS::Point3D> topSrf;
    for(int i=0; i<4; i++) topSrf.push_back(tmp[i]);
    topSrf.push_back(tmp[0]);

    std::vector<drawNS::Point3D> bottomSrf;
    for(int i=4; i<8; i++) bottomSrf.push_back(tmp[i]);
    bottomSrf.push_back(tmp[4]);
    
    int a = api->draw_surface(std::vector<std::vector<drawNS::Point3D>>{topSrf, bottomSrf});
    setId(a);
}
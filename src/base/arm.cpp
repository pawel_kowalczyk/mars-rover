#include "../../inc/base/arm.hh"

Arm::Arm(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, double len) : 
            Cuboid(t_api, parent, MatrixRot(), Vector3D(), len, 1, 1){

    for(Vector3D& vertex : getVertexes()){
        vertex += {len/2, 0, 0};
    }

    rotate(rot);
    translate(translation);
    
}
#include "../../inc/base/simulation.hh"

Simulation::Simulation(int* argc, char** argv){

    api = std::make_shared<drawNS::APIopenGL3D>(-MAP_SIZE_LENGTH,MAP_SIZE_LENGTH, -MAP_SIZE_WIDTH,MAP_SIZE_WIDTH, 0,MAP_SIZE_HEIGHT, 0, argc, argv);

    globalCoords = new CoordSystem(nullptr, MatrixRot(), Vector3D(0,0,0));

    rovers.push_back( new SFJRover(api, globalCoords, MatrixRot(), Vector3D({  0,  0, 1}), "black") );
    rovers.push_back( new Rover   (api, globalCoords, MatrixRot(), Vector3D({ 20, 15, 1}), "black") );
    rovers.push_back( new Rover   (api, globalCoords, MatrixRot(), Vector3D({-20,-12, 1}), "black") );

    onFocus = nullptr;

    probes.push_back(new Regolit(api, globalCoords, MatrixRot(), Vector3D({ 15,  -6, 0.5}), "Regolitot"));
    probes.push_back(new Regolit(api, globalCoords, MatrixRot(), Vector3D({-15,  10, 0.5}), "Marcepano"));
    probes.push_back(new Regolit(api, globalCoords, MatrixRot(), Vector3D({  6, -12, 0.5}), "Monolitot"));
    
    for(Rover* rover : rovers){
        renderable.push_back( dynamic_cast<RenderInterface*  > ( rover ) );
        obstacles.push_back ( dynamic_cast<ObstacleInterface*> ( rover ) );
    }

    for(Regolit* regolit : probes){
        renderable.push_back( dynamic_cast<RenderInterface*  > ( regolit ) );
        obstacles.push_back ( dynamic_cast<ObstacleInterface*> ( regolit ) );
    }
    _running = true;
}

void Simulation::changeRover() {
    unsigned int newFocusId;

    std::cout << "Select Rover (1,2,3): ";
    std::this_thread::sleep_for(500ms);
    highlightRovers();
    
    std::cin >> newFocusId;
    if(newFocusId <= 3){
        onFocus = rovers[newFocusId-1];
        onFocus->setColor("green");
        onFocus->render();
    }
    else{
        std::cerr << "!!!  Invalid rover id  !!!\n" << std::endl;
        std::this_thread::sleep_for(1s);
        if( onFocus != nullptr ){
            onFocus->setColor("green");
            onFocus->render();
        }
    }
}

void Simulation::highlightRovers() const{
    for(Rover* rover : rovers){
        rover->setColor("black");
        rover->render();
    }

    for(Rover* rover : rovers){
        rover->setColor("red");
        rover->render();
        std::this_thread::sleep_for(1000ms);
        rover->setColor("black");
        rover->render();
    }
}

void Simulation::menu() const{
    //system("clear");
    cout << "Pick action \n";
    cout << "1 - Select Rover \n";
    cout << "2 - Drive distance \n";
    cout << "3 - Rotate \n";
    cout << "4 - Exit \n"; 
}

void Simulation::decisionsDrive(){
    cout << "Enter distance: ";
    cin >> distance;
    
    cout << "Enter speed (0 == standard)\n";
    cin >> speed;

    if( speed < 0 )
        cout << endl << " !!! Velocity should be a positive number [ STOP ] !!! " << endl << endl;
    
    else if ( speed == 0 )
        cout << endl << " !!! Auto speed activated [" << STANDARD_ROVER_SPEED << "] !!! " << endl << endl;

}

void Simulation::decisionsRotate(){
    cout << "Enter angle: ";
    cin >> angle;

    cout << "Enter speed (0 == standard)\n";
    cin >> rotSpeed;

    if( rotSpeed == 0 )
        cout << endl <<  "!!! Auto Rotation speed activated [" << STANDARD_ROVER_ROTATION << "] !!! " << endl << endl;

    else if ( rotSpeed < 0 )
        cout << endl << " !!! Rotation speed should be a positive number [ STOP ] !!! " << endl << endl;

}

// void Simulation::update(){
//     onFocus->update();
// }

void Simulation::render(){
    for(RenderInterface* obj : renderable){
        obj->render();
    }
}

bool Simulation::canPickRegolit(Regolit* regolit) const{

    if ( (onFocus->getCenter() - regolit->getCenter()).length() < onFocus->getRadius() + regolit->getRadius() ){
        
        double a = (onFocus->getCenter() - regolit->getCenter()).length();
        double b = MAXFLOAT;

        for(Vector3D wheelPos : onFocus->getCorners()){
            double tmp = (wheelPos - regolit->getCenter()).length();
            b = tmp < b ? tmp : b;
        }
        return (a-b <= REGOLIT_PICK_RANGE);
    }
    
    return false;
}

void Simulation::removeRegolit(Regolit* toRemove){
    int idx=0;
    bool found = false;
    for(RenderInterface* obj : renderable){
        Regolit* tmp = dynamic_cast<Regolit*>(obj);
        if ( tmp != nullptr && tmp->getName() == toRemove->getName()){
            found = true;
            break;
        }
        ++idx;
    }
    if( found )
        renderable.erase(renderable.begin()+idx);

    idx=0;
    found = false;
    for(ObstacleInterface* obj : obstacles){
        Regolit* tmp = dynamic_cast<Regolit*>(obj);
        if( tmp != nullptr && tmp->getName() == toRemove->getName()){
            found = true;
            break;
        }
        ++idx;
    } 
    if( found )
        obstacles.erase(obstacles.begin()+idx);

    idx=0;
    found = false;
    for(Regolit* regolit : probes){
        if( regolit->getName() == toRemove->getName() ){
            found = true;
            break;
        }
        ++idx;
    }   
    if( found )
        probes.erase(probes.begin()+idx);

    api->erase_shape(toRemove->getId());
    delete toRemove;
}

void Simulation::loop(){
    char choice[2];
    render();
    render();

    // for(Regolit* regolit : probes){
    //     cout << "Regolit at " << regolit->getCollisionParameters().center << endl;
    // }
    
    cout << "Press enter to start\n";
    std::cin.get();

    while( onFocus == nullptr )
        changeRover();
    for(Vector3D wheelPos : onFocus->getCorners()){
        cout << wheelPos << endl;
    }
    while(_running){
        cout << "Aktualne: " << Vector3D::akt << endl;
        cout << "Ogolne: " << Vector3D::ogolne << endl;
        menu();
        cin >> choice;

        switch (choice[0]){
        case '1':{
            changeRover();
            break;
        }

        case '2':{
            decisionsDrive();
            onFocus->drive(distance, speed, obstacles);
   
            for( Regolit* obj : probes ){
                if ( dynamic_cast<SFJRover*>(onFocus) != nullptr ){
                    if ( canPickRegolit(obj) ){
                        cout << " !!! Picking Regolit !!! " << endl;
                        std::this_thread::sleep_for(1s);
                        onFocus->drive(5,STANDARD_ROVER_SPEED, {});
                        cout << " !!! You colleted " << obj->getName() << " !!! " << endl << endl;
                        std::this_thread::sleep_for(1s);
                        removeRegolit(obj);
                        // obj->moveAway();
                        // toRemove = dynamic_cast<Regolit*>(obj)->getName();
                        break;
                    }
                    // else{
                    //     cout << " !!! Cant pick Regolit !!! " << endl << endl;
                    // }
                }
                else if( canPickRegolit(obj) ){
                    cout << " !!! non SFJRovers can't pick up Regolit !!! " << endl << endl;
                }
            }
            render();
            break;
        }
        case '3':{
            decisionsRotate();

            onFocus->rotate(angle, rotSpeed);
            break;
        }
        case '4':{
            _running = false;
            break;
        }  
        default:{
            cerr << "!!!  Unknown option  !!!\n";
            break;
        }
        }
    }
}

/*

    Scena:
     - laziki
     - probki
     - przeszkody

    interfejsy:
     - rysowania
     - przeszkody (bool kolizja( lazik ))
     - lazika
     - lazika SFR

    dynamic_case / dynamic_pointer_case

    erase-remove idiom

    czyKolizja, czyMoznaPodniesc -- metoda regolitu

    przelaczenie miedzy interfacami
    std::vector<A*> as;
    std::vector<E*> es;

    as.push_back(new F);
    es.push_back(std::dynamic_cast<E*>(as.back()));

    as[0].foo();
    es[0].bar();

*/

#include "../../inc/base/wheel.hh"

Wheel::Wheel(shared_ptr<drawNS::Draw3DAPI> t_api, CoordSystem* parent, MatrixRot rot, Vector3D translation, double r ) : CoordSystem(parent, rot, translation){
    
    api = t_api;
    
    _r = r;
    setId(-1);
    setColor("black");

    calculateVertexes();
}

void Wheel::calculateVertexes(){
    for(int i=0; i<10; i++){
        double angle = 36*i*M_PI/180;
        _vertexes[i] = Vector3D({cos(angle), sin(angle), 0})*_r;
        _vertexes[i+10] = Vector3D({cos(angle)*_r, sin(angle)*_r, WHEEL_THICKNESS});
    }    
}

double Wheel::getCircumference() const{
    return 2*M_PI*_r;
}

void Wheel::render(){
    if(getId() != -1){
        api->erase_shape(getId());
    }

    std::vector<drawNS::Point3D> tmp;
    for(int i=0; i<20; i++) 
        tmp.push_back(calculateToGlobal(_vertexes[i]).toPoint3D());
    
    std::vector<drawNS::Point3D> topSrf;
    for(int i=0; i<10; i++) topSrf.push_back(tmp[i]);
    topSrf.push_back(tmp[0]);

    std::vector<drawNS::Point3D> bottomSrf;
    for(int i=10; i<20; i++) bottomSrf.push_back(tmp[i]);
    bottomSrf.push_back(tmp[10]);
    
    setId(api->draw_surface(std::vector<std::vector<drawNS::Point3D>>{topSrf, bottomSrf})); 

}
#include "../../inc/utils/vector3D.hh"

Vector3D::Vector3D() : Vector({0,0,0}) {
    ++akt;
    ++ogolne;
}

Vector3D::Vector3D(double a, double b, double c) : Vector({a,b,c}){
    ++akt;
    ++ogolne;    
}

Vector3D::Vector3D(const Vector<double,3>& vector) {
    for(int i=0; i<3; i++) 
        (*this)[i] = vector[i];

    ++akt;
    ++ogolne;
}

Vector3D::Vector3D(std::initializer_list<double> in){
    std::copy(in.begin(), in.end(), getVector().begin());
    
    ++akt;
    ++ogolne;
} 

Vector3D::Vector3D(const Vector3D& vector){
    for(unsigned int i=0; i<3; i++){
        (*this)[i] = vector[i];
    }
    ++akt;
    ++ogolne;
}

Vector3D& Vector3D::operator = (const Vector3D& vector){
    for(int i=0; i<3; i++) 
        (*this)[i] = vector[i];

    return (*this);
}

Vector3D::~Vector3D(){
    --akt;
}

Point3D Vector3D::toPoint3D() const{
    return Point3D((*this)[0], (*this)[1], (*this)[2]);
}
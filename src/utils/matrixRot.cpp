#include "../../inc/utils/matrixRot.hh"

MatrixRot::MatrixRot() : MatrixRot('X', 0){}

MatrixRot::MatrixRot(const Matrix<double,3>& mat) {
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++)
            (*this)[i][j] = mat[i][j];
}

MatrixRot::MatrixRot(std::initializer_list<double> in){
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++)
            (*this)[i][j] = *(in.begin() + j+i*3);
}

MatrixRot::MatrixRot(char axle, double angle){
    switch(axle){
        case 'X':{
            (*this) = {
                1, 0         ,  0         ,
                0, cos(angle), -sin(angle),
                0, sin(angle),  cos(angle) };
            break;
        }
        case 'Y':{
            (*this) = {
                    cos(angle), 0, sin(angle),
                    0         , 1, 0         ,
                -sin(angle), 0, sin(angle) };
            break;
        }
        case 'Z':{
            (*this) = {
                cos(angle), -sin(angle), 0,
                sin(angle),  cos(angle), 0,
                0         ,  0         , 1 };
            break;
        }
        default:{
            std::cerr << "Nieznana os obrotu \n" << std::endl;
            exit(-9);
            break;
        }
    }
}